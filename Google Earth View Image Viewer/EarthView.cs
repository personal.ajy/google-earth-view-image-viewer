﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Google_Earth_View_Image_Viewer
{
	public static class EarthView
	{
		public static string BaseUrl = "https://www.gstatic.com/prettyearth/assets/data/";

		public static string GetRandomId()
		{
			var json = System.IO.File.ReadAllText("imageIds.json");
			var imageIds = JsonConvert.DeserializeObject<ImageIds>(json);

			return imageIds.Ids[new Random().Next(0, imageIds.Ids.Length)];
		}

		public static string GetUrlFromId(string id)
		{
			return BaseUrl + id + ".json";
		}

		public static GstaticResponse GetGstaticContent(string url)
		{
			HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
			req.Method = "get";

			HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
			Stream respStream = resp.GetResponseStream();

			string json;
			using (StreamReader reader = new StreamReader(respStream))
			{
				json = reader.ReadToEnd();
			}

			var gstaticResp = JsonConvert.DeserializeObject<GstaticResponse>(json);

			return gstaticResp;
		}

	}
}
