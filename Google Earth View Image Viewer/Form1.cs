﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Threading;

namespace Google_Earth_View_Image_Viewer
{
	public partial class Form1 : Form
	{
		string baseUrl = "https://www.gstatic.com/prettyearth/assets/data/";

		public Form1()
		{
			InitializeComponent();

			button1.Click += Button1_Click;
			button2.Click += Button2_Click;

			progressBar1.Visible = false;
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			pictureBox1.Image.Save("download.jpg");

			MessageBox.Show("Saved!");
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			progressBar1.Visible = true;

			var thread = new Thread(new ThreadStart(DownloadImage));
			thread.Start();
		}

		public void DownloadImage()
		{
			var imgUrl = EarthView.GetUrlFromId(EarthView.GetRandomId());
			var response = EarthView.GetGstaticContent(imgUrl);

			var imgBase64 = response.DataUri.Substring(response.DataUri.IndexOf(",") + 1);
			byte[] imgBytes = Convert.FromBase64String(imgBase64);

			DownloadedImage(Helpers.ByteArrayToImage(imgBytes));
		}

		public delegate void DownloadedImageCallback(Image image);

		public void DownloadedImage(Image image)
		{
			if (InvokeRequired)
			{
				DownloadedImageCallback callback = new DownloadedImageCallback(DownloadedImage);
				this.Invoke(callback, image);
			}
			else
			{
				pictureBox1.Image = image;
				progressBar1.Visible = false;
			}
		}
	}

	public class ImageIds
	{
		[JsonProperty("ids")]
		public string[] Ids;
	}

	public class GstaticResponse
	{
		[JsonProperty("dataUri")]
		public string DataUri;
	}
}
